-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Nathan DUBURCQ
-- Caption:       New Model
-- Project:       We Are Hiring
-- Changed:       2022-03-01 14:15
-- Created:       2022-03-01 14:15
PRAGMA foreign_keys = OFF;

-- Schema: mydb
BEGIN;
CREATE TABLE "Message"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "mail" VARCHAR(45) NOT NULL,
  "content" LONGTEXT NOT NULL
);
CREATE TABLE "Offer"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" VARCHAR(45) NOT NULL,
  "title" VARCHAR(45) NOT NULL,
  "created_at" VARCHAR(45) NOT NULL,
  "type" VARCHAR(45) NOT NULL,
  "location" VARCHAR(45) NOT NULL,
  "job" VARCHAR(45) NOT NULL
);
CREATE TABLE "Apply"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "mail" VARCHAR(45) NOT NULL,
  "firstname" VARCHAR(45) NOT NULL,
  "lastname" VARCHAR(45) NOT NULL,
  "address" VARCHAR(45),
  "phone" VARCHAR(45),
  "linkedin" VARCHAR(45),
  "git" VARCHAR(45),
  "Offer_id" INTEGER NOT NULL,
  CONSTRAINT "fk_Apply_Offer"
    FOREIGN KEY("Offer_id")
    REFERENCES "Offer"("id")
);
CREATE INDEX "Apply.fk_Apply_Offer_idx" ON "Apply" ("Offer_id");
COMMIT;
