import './App.css'
import Contact from './pages/contact'
import Offers from './pages/offers'
import Home from './pages/home'
import MyApplication from './pages/myapplication'
import Register from './pages/register';
import { useState } from 'react';
import { Header, Navbar, Footer } from './components/common'
import Login from './pages/login';
import Favorite from './pages/favorite';

function App() {
  let [page, setpage] = useState('Home');
  let [loged, setloged] = useState('')
  function gologed(value){
    setloged(value)
  }

  function Switch(pageName) {
    setpage('')
    setTimeout(() => {
      setpage(pageName)
    }, 10)
  }

      if (page==='Contact'){
      return(
        <div className='container-general'>
        <Navbar Switch={Switch} loged={loged} gologed={gologed}/>
      <Header titre = {"Contact Us"} />
      <Contact Switch={Switch}/>
      <Footer />
      </div>
      )}
      if (page==='Offers'){
      return(
        <div className='container-general'>
        <Navbar Switch={Switch} loged={loged} gologed={gologed}/>
      <Offers Switch={Switch} />
      <Footer />
      </div>
    )
  }

  if (page === 'MyApplication' && loged !=='') {
    return (
      <div>
        <Navbar Switch={Switch}  loged={loged} gologed={gologed}/>
        <Header titre={'My applications'} />
        <MyApplication Switch={Switch} />
        <Footer />
      </div>
    )
  }

  if (page === 'Home') {
    return (
      <div>
        <Navbar Switch={Switch} loged={loged} gologed={gologed} />
        <Home Switch={Switch} />
        <Footer />
      </div>
        )
      }if (page === 'Register' && loged == ''){
        return(
        <div>
          <Navbar Switch={Switch} loged={loged} gologed={gologed}/>
          <Header titre={'Sign up'}/>
          <Register Switch={Switch} gologed={gologed}/>
          <Footer />
        </div>
        )
      }
      if (page === 'Login' && loged == ''){
        return(
        <div>
          <Navbar Switch={Switch} loged={loged} gologed={gologed}/>
          <Header titre={'Sign In'}/>
          <Login Switch={Switch} gologed={gologed}/>
          <Footer />
        </div>
        )
      }
      if (page === 'Favorite' && loged !== ''){
        return(
        <div>
          <Navbar Switch={Switch} loged={loged} gologed={gologed}/>
          <Favorite Switch={Switch} loged={loged}/>
          <Footer />
        </div>
        )
      }
      else{
        return(<div></div>)
      }
}

export default App
