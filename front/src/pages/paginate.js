import { useEffect, useState } from 'react'
import '../App.css'
import Card from '../components/common/card'
import SingleCard from './SingleCard'

function Paginate(props) {
  let [pageNb, setpageNb] = useState(1)
  let [data, setdata] = useState()
  let [mod, setmod] = useState('Offers')
  let [carddata, setcarddata] = useState()
  let [previousState, setpreviousState] = useState('prev-btn-disabled')
  let [nextState, setnextState] = useState('prev-btn-disabled')

  function incrementPage() {
    if (pageNb < Math.ceil(props.Data.length / 9)) {
      setpageNb(pageNb + 1)
    }
  }
  function decrementPage() {
    if (pageNb > 1) {
      setpageNb(pageNb - 1)
    }
  }

  function changePreviousButton() {
    if (pageNb > 1) {
      setpreviousState('prev-btn')
    } else {
      setpreviousState('prev-btn-disabled')
    }
  }

  function changeNextButton() {
    if (pageNb < Math.ceil(props.Data.length / 9)) {
      setnextState('prev-btn')
    } else {
      setnextState('prev-btn-disabled')
    }
  }

  function getData() {
    let arrayOfData = []
    let i = 0
    while (i < 9) {
      if (props.Data[i + (pageNb - 1) * 9] !== undefined) {
        arrayOfData.push(props.Data[i + (pageNb - 1) * 9])
      }
      i++
    }
    let content = arrayOfData.map((e) => (
      <Card key={e.id} objet={e} Seemore={Seemore} />
    ))

    setdata(content)
  }

  function Seemore(dta) {
    setcarddata(dta)
    setmod('Offer')
  }

  function goBack() {
    setmod('Offers')
  }

  useEffect(() => {
    getData()
    changeNextButton()
    changePreviousButton()
  }, [pageNb])

  if (mod === 'Offers') {
    return (
      <div>
        <div className="header">
          <p>Offers</p>
        </div>
        <div className="testgrid">{data}</div>
        <div className="next-prev-btn">
          <button className={previousState} onClick={decrementPage}>
            {'<<'} Previous Page
          </button>
          <button className={nextState} onClick={incrementPage}>
            Next Page {'>>'}
          </button>
        </div>
      </div>
    )
  }
  if (mod === 'Offer') {
    return (
      <div>
        <SingleCard objet={carddata} goBack={goBack} />
      </div>
    )
  }
}

export default Paginate
