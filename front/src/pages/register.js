import { useState } from 'react'
import '../App.css'

function Register(props) {
  let [email, setemail] = useState('')
  let [password, setpassword] = useState('')
  let [passwordConfirm, setpasswordConfirm] = useState('')

  function Cemail(event) {
    setemail(event.target.value)
  }

  function Cpassword(event) {
    setpassword(event.target.value)
  }

  function CpasswordConfirm(event) {
    setpasswordConfirm(event.target.value)
  }

  function adduser(e) {
    e.preventDefault();
    fetch('http://localhost:8000/api/register', {
      method: 'POST',
      body: new URLSearchParams({
        email: email,
        password: password,
        passwordConfirm: passwordConfirm,
      })
    }).then(response => response.json().then(resJson => props.gologed(resJson.token) ).then(props.Switch('Home'))
    )
  }

  // changement quand backend
  return (
    <div className="divfor">
      <form
        onSubmit={adduser}
      >
        <div className="csgo">
          <label className="lab" htmlFor="email">
            Email
          </label>
          <input
            className="champs"
            onChange={Cemail}
            type={'email'}
            required={true}
          ></input>

          <label className="lab" htmlFor="password">
            Password
          </label>
          <input
            className="champs"
            onChange={Cpassword}
            type={'password'}
            required={true}
          ></input>

          <label className="lab" htmlFor="password">
            Confirm password
          </label>
          <input
            className="champs"
            onChange={CpasswordConfirm}
            type={'password'}
            required={true}
          ></input>
        </div>
        <div className="divform">
        <div className="space"></div>
        <button className="home-btn">Sign Up</button>
        <div className="minspace"></div>
        </div>
      </form>

      <div className="divform">
        <p className="signi">You already have an account yet?</p>
        <a className="signi" href="#" onClick={() => props.Switch('Login')}>
          Sign In
        </a>
      </div>
    </div>
  )
}

export default Register
