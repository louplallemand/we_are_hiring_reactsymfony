import React from 'react'
import '../App.css'
import './myapplication.css'

export default class MyApplication extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      applications: [],
      affichage: [],
    }
  }

  async componentDidMount() {
    let data = []
    const requete = await fetch('http://localhost:8000/api/application?', {
      method: 'GET',
    })

    if (!requete.ok) {
      console.log('Un problème est survenu.')
    } else {
      let donnees = await requete.json()

      // recupere toutes les offres
      fetch(`http://localhost:8000/api/offers`).then((response) =>
        response.json().then((offres) => {
          let i = 0
          let months = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
          ]
          donnees
            .map((donnee) => Number(donnee.offer_id))
            .forEach((offer_id) => {
              const offer = offres.find((offre) => offre.id === offer_id)

              let monthNb = 0
              if (months.indexOf(donnees[i].date.split(' ')[1]) + 1 < 10) {
                monthNb =
                  '0' + (months.indexOf(donnees[i].date.split(' ')[1]) + 1)
              } else {
                monthNb = months.indexOf(donnees[i].date.split(' ')[1]) + 1
              }
              let DateMDY = `${monthNb}/${donnees[i].date.split(' ')[2]}/${
                donnees[i].date.split(' ')[3]
              }`

              data.push({
                id: offer.id,
                titre: offer.titre,
                type: offer.type,
                ville: offer.ville,
                date: DateMDY,
              })
              i++
            })
          this.setState({ applications: data })

          let provisoire = []
          this.state.applications.forEach((e) => {
            provisoire.push(
              <div key={e.id} className="border">
                <p className="date">Applied on {e.date}</p>
                <p className="titre">{e.titre}</p>
                <p className="alignement">
                  {e.type}-{e.ville}
                </p>
              </div>,
            )
          })

          this.setState({ affichage: provisoire })
        }),
      )
    }
  }

  render() {
    return <div className="bordurer">{this.state.affichage}</div>
  }
}
