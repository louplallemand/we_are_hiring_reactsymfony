import { useEffect, useState } from 'react'
import '../App.css'
import Paginate from './paginate'

function Offers(props) {
  let [Data, setData] = useState()

  function getOffers() {
    let api = 'http://localhost:8000'
    fetch(`${api}/api/offers`).then((response) =>
      response.json().then((data) => {
        setData(data.details)
      }),
    )
  }

  useEffect(() => {
    getOffers()
  }, [])

  if (Data !== undefined) {
    return (
      <div className="pageoffers">
        <Paginate Data={Data} Switch={props.Switch} />
      </div>
    )
  }
  return <div></div>
}

export default Offers
