import { useEffect, useState } from 'react'
import { Card, Header } from '../components/common'
import SingleCard from './SingleCard'

function Favorite(props) {
  let [data, setdata] = useState([])
  let [mod, setmod] = useState('Favorite')
  let [carddata, setcarddata] = useState()

  function getFav() {
    console.log(props.loged)
    let api = 'http://localhost:8000'
    fetch(`${api}/api/favorites`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${props.loged}` // notice the Bearer before your token
      }
    }).then((response) =>
      response.json().then((data) => {
        fetch(`${api}/api/offers`).then((response) =>
          response.json().then((dta) => {
            let arrayOfData = []
            let arrayOfHtml = []
            data
              .map((e) => Number(e.offers_id))
              .forEach((offer_id) => {
                const offer = dta.find((e) => e.id === offer_id)
                arrayOfData.push(offer)
              })
            arrayOfData.forEach((e) => {
              arrayOfHtml.push(<Card key={e.id} objet={e} Seemore={Seemore} />)
            })
            setdata(arrayOfHtml)
          }),
        )
      }),
    )
  }

  function Seemore(dta) {
    setcarddata(dta)
    setmod('Offer')
  }

  function goBack() {
    setmod('Favorite')
  }

  useEffect(() => {
    getFav()
  }, [])

  if (mod === 'Favorite') {
    return (
      <div>
        <Header titre={'Favorite offers'} />
        <div className="pageoffers">
          <div className="testgrid">{data}</div>
        </div>
      </div>
    )
  }
  if (mod === 'Offer') {
    return (
      <div>
        <SingleCard objet={carddata} goBack={goBack} />
      </div>
    )
  }
}

export default Favorite
