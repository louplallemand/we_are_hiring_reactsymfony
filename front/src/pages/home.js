import React from 'react'
import './home.css'
import Card from '../components/common/card'
import SingleCard from './SingleCard'

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      offres: [],
      users: [],
      mod: 'Home',
      carddata: [],
    }
    this.Seemore = this.Seemore.bind(this)
    this.goBack = this.goBack.bind(this)
  }

  // async componentDidMount() {
  //   const requete = await fetch('http://localhost:8000/api/offers', {
  //     method: 'GET',
  //   })

  //   if (!requete.ok) {
  //     console.log('Un problème est survenu.')
  //   } else {
  //     let donnees = await requete.json()
  //     this.setState({
  //       offres: donnees,
  //     })
  //   }
  // }

  componentDidMount() {
    let api = 'http://localhost:8000'
    fetch(`${api}/api/offers`).then((response) =>
      response.json().then((data) => {
        this.setState({
          offres: data.details,
        })
      }),
    )
  }

  Seemore(dta) {
    this.setState({
      mod: 'Offer',
      carddata: dta,
    })
  }

  goBack() {
    this.setState({
      mod: 'Home',
    })
  }

  render() {
    let dernierElem = this.state.offres[this.state.offres.length - 1]
    let deuxiemeDernierElem = this.state.offres[this.state.offres.length - 2]
    let troisDernierElem = this.state.offres[this.state.offres.length - 3]

    if (dernierElem === undefined) {
      return <div></div>
    }

    if (this.state.mod === 'Home') {
      return (
        <div className="home">
          <div className="header-home">
            <div className="title-header">
              <h1>Finding a job has never been easier</h1>
            </div>
            <div className="we-hiring">
              <p>We hiring allows you to find the best job offers on the market and apply quickly and securely.</p>
            </div>
            <div className="see-all">
              <button
                onClick={() => this.props.Switch('Offers')}
                className="home-btn"
              >
                See all offers
              </button>
            </div>
          </div>
          <div className="aboutUs">
            <h2>About Us</h2>
            <p className="afterabout">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod temporem ipsum dolor sit amet, consectetur adipiscing
              elit, sed do eiusmod tempor
            </p>
          </div>
          <div className="lastOffers">
            <div className="lastoffercentre">
              <h2 className="title-last">Last offers</h2>
            </div>
            <div className="last-cards">
              <div className="card-1">
                <Card objet={dernierElem} Seemore={this.Seemore} />
              </div>
              <div className="card-2">
                <Card objet={deuxiemeDernierElem} Seemore={this.Seemore} />
              </div>
              <div className="card-3">
                <Card objet={troisDernierElem} Seemore={this.Seemore} />
              </div>
            </div>
          </div>
          <div className="ready-contact">
            <div className="ready">
              <h2 className="started">Ready to get started?</h2>
              <h2 className="contact">Sign up or contact us</h2>
            </div>
            <div className="contact-us">
              <button
                onClick={() => {
                  this.props.Switch('Register')
                }} className="home-btn">
                Sign Up
              </button>
              <button
                className="btn-contact"
                onClick={() => {
                  this.props.Switch('Contact')
                }}
              >
                Contact Us
              </button>
            </div>
          </div>
        </div>
      )
    }

    if (this.state.mod === 'Offer') {
      return (
        <div>
          <SingleCard objet={this.state.carddata} goBack={this.goBack} />
        </div>
      )
    }
  }
}
