import { useState } from 'react'
import '../App.css'

function Login(props) {
  let [email, setemail] = useState('')
  let [password, setpassword] = useState('')

  function Cemail(event) {
    setemail(event.target.value)
  }

  function Cpassword(event) {
    setpassword(event.target.value)
  }

  function getuser(e) {
    e.preventDefault();
    fetch('http://localhost:8000/api/login', {
      method: 'POST',
      body: new URLSearchParams({
        email: email,
        password: password,
      })
    }).then(response => response.json().then(resJson => props.gologed(resJson.token) ).then(props.Switch('Home'))
    )
  }

  return (
    <div>
      <form
        onSubmit={getuser}
      >
        <div className="divfor">
          <label className="lab" htmlFor="email">
            Email
          </label>
          <input
            className="champs"
            onChange={Cemail}
            type={'email'}
            required={true}
          ></input>

          <label className="lab" htmlFor="password">
            Password
          </label>
          <input
            className="champs"
            onChange={Cpassword}
            type={'password'}
            required={true}
          ></input>
          <div className="divform">
            <div className="minspace"></div>
            <button className="home-btn">Sign In</button>
            <div className="minspace"></div>
            <p className="signi">Don't have an account yet?</p>
        <a className="signi" href="#" onClick={() => props.Switch('Register')}>
          Create an account
        </a>
        <div className="space"></div>
      <div className="space"></div>
          </div>
        </div>
      </form>
    </div>
  )
}

export default Login
