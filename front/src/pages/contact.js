import { useState } from 'react'
import '../App.css'

function Contact(props) {
  let [mod, setmod] = useState('Form')
  let [email, setemail] = useState('')
  let [message, setmessage] = useState('')

  function Cemail(event) {
    setemail(event.target.value)
  }

  function Cmessage(event) {
    setmessage(event.target.value)
  }

  function changemod(mod) {
    setmod(mod)
  }

  function SubmitMessage() {
    let api = 'http://localhost:8000'
    fetch(`${api}/api/contact`, {
      method: 'POST',
      body: new URLSearchParams({ email: email, message: message }),
    })
  }

  if (mod === 'Form') {
    return (
      <div className="form-box">
        <form onSubmit={() => (changemod('Thx'), SubmitMessage())}>
          <div className="labelo">
            <label className="lab" htmlFor="email">Your email address</label>
          </div>
          <input
            onChange={Cemail}
            required={true}
            itemType="text"
            id="email"
          ></input>
          <div className="labelo">
            <label className="lab" htmlFor="message">Your message</label>
          </div>
          <textarea
            rows={15}
            required={true}
            onChange={Cmessage}
            id="message"
          ></textarea>
          <div className="btn-center">
            <button className="home-btn" type="submit">
              Send
            </button>
          </div>
        </form>
      </div>
    )
  }
  if (mod === 'Thx') {
    return (
      <div className="thanksyour">
        <h1 className="thanks">Thanks !</h1>
        <p className="beensent">Your message has been sent !</p>
        <p className="wethank">We thank you for your attention.</p>
        <p className="wewill">We will respond as soon as possible.</p>
        <p className="seeyou">See you soon</p>
      </div>
    )
  }
}

export default Contact
