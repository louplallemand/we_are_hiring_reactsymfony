import React, { useState } from 'react'
import FormApply from '../components/specific/FormApply'
import Star from '../components/common/star'

function SingleCard(props) {
  let [mod, setmod] = useState('Offer')

  function Toapply() {
    setmod('Apply')
  }
  if (mod === 'Offer') {
    return (
      <div>
        <div className="header">
          <p>{props.objet.titre}</p>
        </div>
        <div className="cards">
          <div className="starisborn">
            <Star offers_id={props.objet.id} />
          </div>
          <div className="job-loc">
            <p className="card_fonc">{props.objet.fonction}</p>
            <div className="loc">
              <p className="card_job">{props.objet.type}</p>
              <p className="card_location">{props.objet.ville}</p>
            </div>
          </div>
          <div className="card_body">
            <p className="card_descr">{props.objet.description}</p>
            <p className="card_dates">Published on {props.objet.parution}</p>
          </div>
        </div>
        <div className="apply-rtr">
          <button
            className="home-btn"
            onClick={() => {
              Toapply()
            }}
          >
            Apply now
          </button>
          <button
            className="prev-btn"
            onClick={() => {
              props.goBack()
            }}
          >
            {'<'} back to offers
          </button>
        </div>
      </div>
    )
  }
  if (mod === 'Apply') {
    return (
      <div>
        <div className="header">
          <p>Apply</p>
        </div>
        <FormApply objet={props.objet} />
      </div>
    )
  }
}

export default SingleCard
