import React from 'react'
import { useState } from 'react'

import './Navbar.css'

function Navbar(props) {
  const [showLinks,setShowLinks] = useState(false) /*Qui nous permet de savoir si c'est fermé ou ouvert*/

    const handleShowLinks = () => {
      setShowLinks(!showLinks) // On va éditer la valeur si le showlinks est different de false.
    }
  return (
    <div>
      <div className="navbar">
      <a
        onClick={() => {
          props.Switch('Home')
        }}
        className="navbar-name"
      >
        WE ARE HIRING
      </a>
      <div className="navbar-items">
        <a
          onClick={() => {
            props.Switch('Home')
          }}
          className="navbar-item"
        >
          Home
        </a>
        <a
          onClick={() => {
            props.Switch('Offers')
          }}
          className="navbar-item"
        >
          Offers
        </a>
        {props.loged !== '' ? (
          //les liens vers my application et favorite offers a mettre ici
          <div className="lognav">
            <a
              onClick={() => {
                props.Switch('MyApplication')
              }}
              className="navbar-item"
            >
              My applications
            </a>
            <a
              className="navbar-item"
              onClick={() => {
                props.Switch('Favorite')
              }}
            >
              Favorites
            </a>
            
          </div>
        ) : (
          <a
            className="navbar-item"
            onClick={() => {
              props.Switch('Login')
            }}
          >
            Login
          </a>
        )}

        <a
          onClick={() => {
            props.Switch('Contact')
          }}
          className="navbar-item"
        >
          Contact
        </a>

        {props.loged !== '' ? (
          <button
            className="navbar-btn"
            onClick={() => {
              props.gologed('')
              props.Switch('Home')
            }}
          >
            Log Out
          </button>
        ) : (
          <button
            className="navbar-btn"
            onClick={() => {
              props.Switch('Register')
            }}
          >
            Sign Up
          </button>
        )}
      </div>
      </div>
      <div className='showM'>
        <nav className={`navbarM ${showLinks ? "show-nav" : "hide-nav" }`}>
        <div className="navbar__logo"><a className="navbar_link" onClick={() => {props.Switch('Home')}}>WE ARE HIRING</a></div>
        <ul className="navbar_links">
          <li className="navbar__item"><a className="navbar_link" onClick={() => {props.Switch('Home')}}>Home</a></li>
          <li className="navbar__item"><a className="navbar_link" onClick={() => { props.Switch('Offers')}}>Offers</a></li>
          {props.loged !== '' ? (
          //les liens vers my application et favorite offers a mettre ici
            <div>
              <li className="navbar__item"><a className="navbar_link" onClick={() => {props.Switch('MyApplication')}}>My applications</a></li>
              <li className="navbar__item"><a className="navbar_link" onClick={() => {props.Switch('Favorite')}}>Favorites</a></li>
            </div>
          ) : (
            <li className="navbar__item"><a className="navbar_link" onClick={() => {props.Switch('Login')}}>Login</a></li>
          )}
          <li className="navbar__item"><a className="navbar_link" onClick={() => {props.Switch('Contact')}}>Contact</a></li>
          {props.loged !== '' ? (
            <li className="navbar__item"><a className="navbar_link" onClick={() => {
              props.gologed('')
              props.Switch('Home')}}>Log Out</a></li>
        ) : (
          <li className="navbar__item"><a className="navbar_link" onClick={() => {props.Switch('Register')}}>Sign Up</a></li>
          )}
        </ul>
        </nav>
        <button className="navbar__burger" onClick={handleShowLinks}>
          <span className="burger-bar"></span>
        </button>
      </div>
    </div>
    
  )
}

export default Navbar
