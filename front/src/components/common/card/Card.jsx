import React from 'react'
import './Card.css'
import '../../../pages/offers'
import Star from '../star/Star'

function Card(props) {
  return (
    <div className="card">
      <div className="card_body">
        <div className="star-date">
          <p className="card_date">{props.objet.parution}</p>
          <div className="starisborn">
            <Star offers_id={props.objet.id} />
          </div>
        </div>
        <h2 className="card_title">{props.objet.titre}</h2>
        <p className="card_description">{props.objet.description}</p>
        <div className="align-btn">
          <p className="card_location">
            {props.objet.ville} - {props.objet.type}
          </p>
          <button
            className="btn-card"
            onClick={() => {
              props.Seemore(props.objet)
            }}
          >
            See More {'>'}
          </button>
        </div>
      </div>
    </div>
  )
}

export default Card
