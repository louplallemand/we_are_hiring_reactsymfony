import React from 'react'

import './Footer.css'

function Footer() {
  return (
    <div className="footer">
      <h3>WE ARE HIRING</h3>
      <hr className="separator" />
      <p>© 2022 - We Are Hiring</p>
    </div>
  )
}

export default Footer
