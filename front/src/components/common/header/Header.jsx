import React from 'react'

import './Header.css'

function Header(props) {
  return (
    <div className="header">
      <p>{props.titre}</p>
    </div>
  )
}

export default Header
