import { useEffect, useState } from 'react'
import './Star.css'
import starem from './starem.png'
import starfu from './starfu.png'

function Star(props) {
  const [imgUrl, setImgUrl] = useState(starem)

  function SubmitFav() {
    if (imgUrl == starem) {
      let api = 'http://localhost:8000'
      fetch(`${api}/api/favorites`, {
        method: 'POST',
        body: new URLSearchParams({ user_id: 1, offers_id: props.offers_id }),
      })
    }
  }

  function getFav() {
    let api = 'http://localhost:8000'
    fetch(`${api}/api/favorites?offers_id=${props.offers_id}`).then((response) =>
      response.json().then((data) => {
        if (data.length !== 0) {
          setImgUrl(starfu)
        }
      }),
    )
  }

  useEffect(() => {
    getFav()
  }, [])

  return (
    <div className="star">
      <button
        onClick={() => setImgUrl(starfu) & SubmitFav()}
        className="btn-star"
      >
        <img src={imgUrl} alt="logo" />
      </button>
    </div>
  )
}

export default Star
