import React from 'react'
import { useState } from 'react'
import './FormApply.css'

export default function FormApply(props) {
  let [mod, setmod] = useState('Form')
  let [firstname, setfirstname] = useState('')
  let [lastname, setlastname] = useState('')
  let [email, setemail] = useState('')
  let [phone, setphone] = useState('')
  let [address, setaddress] = useState({
    street: 'na',
    block: 'na',
    code: 'na',
    city: 'na',
  })
  let [linkedin, setlinkedin] = useState('')
  let [git, setgit] = useState('')

  function Cfirstname(event) {
    setfirstname(event.target.value)
  }

  function Clastname(event) {
    setlastname(event.target.value)
  }

  function Cemail(event) {
    setemail(event.target.value)
  }

  function Cphone(event) {
    setphone(event.target.value)
  }

  function Clinkedin(event) {
    setlinkedin(event.target.value)
  }

  function Cgit(event) {
    setgit(event.target.value)
  }

  function Cstreet(event) {
    setaddress({
      street: event.target.value,
      block: address.block,
      code: address.code,
      city: address.city,
    })
  }

  function Cblock(event) {
    setaddress({
      street: address.street,
      block: event.target.value,
      code: address.code,
      city: address.city,
    })
  }

  function Ccode(event) {
    setaddress({
      street: address.street,
      block: address.block,
      code: event.target.value,
      city: address.city,
    })
  }

  function Ccity(event) {
    setaddress({
      street: address.street,
      block: address.block,
      code: address.code,
      city: event.target.value,
    })
  }

  function changemod(mod) {
    setmod(mod)
  }

  function SubmitApply() {
    fetch('http://localhost:3004/application', {
      method: 'POST',
      body: new URLSearchParams({
        offer_id: props.objet.id,
        firstname: firstname,
        lastname: lastname,
        email: email,
        phone: phone,
        address: JSON.stringify(address),
        linkedin: linkedin,
        git: git,
        date: Date(),
      }),
    })
  }

  if (mod === 'Form') {
    return (
      <div className="divapply">
        <form
          className="divform"
          onSubmit={() => (changemod('Thx'), SubmitApply())}
        >
          <div className="gobtout">
            <div className="applytos">
              <h1>Apply to</h1>
              <h2 className="obj-tit">{props.objet.titre}</h2>
            </div>

            <div className="first">
              <label className="lab" htmlFor="firstname">
                Your firstname<span className="oblig">*</span>
              </label>
              <input
                className="champs"
                rows={15}
                onChange={Cfirstname}
                type="text"
                placeholder="Edy"
                {...('Your First name', { required: true, maxLength: 80 })}
              />
            </div>

            <div className="last">
              <label className="lab" htmlFor="lastname">
                Your lastname<span className="oblig">*</span>
              </label>
              <input
                className="champs"
                onChange={Clastname}
                type="text"
                placeholder="Murphy"
                {...('Your Last name', { required: true, maxLength: 100 })}
              />
            </div>

            <div className="mail">
              <label className="lab" htmlFor="email">
                Your email address<span className="oblig">*</span>
              </label>
              <input
                className="champs"
                onChange={Cemail}
                type="text"
                placeholder="edy.murphy@gmail.com"
                {...('Your email address', { required: true })}
              />
            </div>

            <div className="tel">
              <label className="lab" htmlFor="phone">
                Your phone number
              </label>
              <input
                className="champs"
                onChange={Cphone}
                type="text"
                placeholder="+33 6 13 78 09 78"
              />
            </div>

            <div className="adres1">
              <label className="lab" htmlFor="streetAddres">
                Your address
              </label>
              <input
                className="champs"
                onChange={Cstreet}
                type="text"
                placeholder="775 alabama street"
              ></input>
              <input
                className="champs"
                onChange={Cblock}
                type="text"
                placeholder="Block C"
              ></input>
            </div>

            <div className="adres2">
              <input
                className="codpos"
                onChange={Ccode}
                type="text"
                placeholder="31000"
              ></input>
              <input
                className="cit"
                onChange={Ccity}
                type="text"
                placeholder="Toulouse"
              ></input>
            </div>

            <div className="linked">
              <label className="lab" htmlFor="linkedin">
                Your linkedin profil
              </label>
              <input
                className="champs"
                onChange={Clinkedin}
                type="url"
                placeholder="https://..."
              />
            </div>

            <div className="githu">
              <label className="lab" htmlFor="git">
                Your GitHub / GitLab profil
              </label>
              <input
                className="champs"
                onChange={Cgit}
                type="url"
                placeholder="https://..."
              />
            </div>
          </div>

          <button className="home-btn" type="submit">
            Apply
          </button>
        </form>
      </div>
    )
  }

  if (mod === 'Thx') {
    return (
      <div className="thanksyour">
        <h1 className="thanks">Thanks !</h1>
        <p className="beensent">
          Your application to "{props.objet.titre}" has been sent !
        </p>
        <p className="wethank">We thank you for your application.</p>
        <p className="wewill">We will respond as soon as possible.</p>
        <p className="seeyou">See you soon</p>
      </div>
    )
  }
}
