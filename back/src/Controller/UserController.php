<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;



class UserController extends AbstractController
{
    /**
     * @param Security $security
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function getTokenUser(Security $security, JWTTokenManagerInterface $JWTManager)
    {
        $user = new User('$username', '$password');
        return $this->json(['token' => $JWTManager->create($user)]);
    }

    #[Route('/api/register', name: 'create_user')]
    /**
     * @param Security $security
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function createUser(ManagerRegistry $doctrine, Security $security, JWTTokenManagerInterface $JWTManager): Response
    {
        $request = Request::createFromGlobals();

        if ($request->request->get('password') == $request->request->get('passwordConfirm')){
        $entityManager = $doctrine->getManager();
        $newUser = new User();
        $newUser->setUsername($request->request->get('email'));
        $newUser->setPassword($request->request->get('password'));
        $entityManager->persist($newUser);
        $entityManager->flush();
        $token = $JWTManager->create($newUser);
        return $this->json(['token' => $token]);
        }else{
        return $this->json(['token' => '']);
        }
    }

    #[Route('/api/login', name: 'create_user')]
    /**
     * @param Security $security
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function SignUser(ManagerRegistry $doctrine, Security $security, JWTTokenManagerInterface $JWTManager): Response
    {
        $request = Request::createFromGlobals();
        $entityManager = $doctrine->getManager();
        $usercheck = $doctrine->getRepository(User::class)->findOneBy(['username' => $request->request->get('email')]);
        
        if ($request->request->get('email') == $usercheck->getUsername() && $request->request->get('password') == $usercheck->getPassword()){
            $newUser = new User();
            $newUser->setUsername($request->request->get('email'));
            $newUser->setPassword($request->request->get('password'));
            $token = $JWTManager->create($newUser);
            return $this->json(['token' => $token]);
        }else{
            return $this->json(['token' => '']);
        }
    }

}
