<?php

namespace App\Controller;
use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $request = Request::createFromGlobals();
        $entityManager = $doctrine->getManager();
        $newFormContact = new Contact();
        $newFormContact->setEmail($request->request->get('email'));
        $newFormContact->setMessage($request->request->get('message'));
        $entityManager->persist($newFormContact);
        $entityManager->flush();
        return $this->json([
            'message' => 'contact ok',
            'email' => 'cestpasunmailbatramos',
            'path' => 'src/Controller/ContactController.php',
        ]);
    }
}
