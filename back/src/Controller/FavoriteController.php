<?php

namespace App\Controller;

use App\Entity\Offers;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class FavoriteController extends AbstractController
{
    #[Route('/api/favorites', name: 'app_favorite')]
    public function index(TokenStorageInterface $tokenStorageInterface, JWTTokenManagerInterface $jwtManager): Response
    {
        // $request = Request::createFromGlobals();
        // $t = json_decode($request);

        $this->jwtManager = $jwtManager;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
        
        return $this->json(['token' => $decodedJwtToken], Response::HTTP_OK);
        //return new Response ($decodedJwtToken, Response::HTTP_OK);
    }
}
