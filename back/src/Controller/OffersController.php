<?php

namespace App\Controller;

use App\Entity\Offers;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

class OffersController extends AbstractController
{
#[Route('/api/offers', name: 'offers_list')]
    public function showAll(ManagerRegistry $doctrine): Response
    {

        $offers = $doctrine->getRepository(Offers::class)->findAll();
        foreach($offers as $item) {
            $arrayCollection[] = array(
                'id' => $item->getId(),
                'title'=> $item->getTitre(),
                'type'=>$item->getType(),
                'fonction'=>$item->getFonction(),
                'description'=>$item->getDescription(),
                'ville'=>$item->getVille(),
                'date'=>$item->getParution(),
            );
       }

        return $this->json(["details" => $arrayCollection], Response::HTTP_OK);
    }}
